version: "3.8"

services:
  client:
    image: $ECR_REPO/ecr-front:latest
    environment:
      - REACT_APP_BACKEND_URL=http://$PUBLIC_IP:5000/api
    ports:
      - "80:3000"
      
  server:
    image: $ECR_REPO/ecr-back:latest
    ports:
      - "5000:5000"
    environment:
      - MONGODB_URI=${MONGODB_URI}
      - TOKEN_KEY=${TOKEN_KEY}
      - EMAIL=${EMAIL}
      - PASSWORD=${PASSWORD}