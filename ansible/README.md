# Ansible Deployment for Docker Compose Services

This Ansible playbook deploys a Docker Compose-based application with a frontend and backend service on an EC2 instance.

## Prerequisites

- Ansible installed on your local machine
- AWS CLI installed and configured with appropriate credentials
- An EC2 instance running with a public IP address and access via SSH key

## Usage

1. Clone the repository to your local machine.

2. Set the environment variables for the playbook (retieve with CI/CD variables in our pipeline):

```bash
export MONGODB_URI=<your_mongodb_uri>
export TOKEN_KEY=<your_token_key>
export EMAIL=<your_email>
export PASSWORD=<your_password>
```

## Files

- `docker-compose.yml.tpl`: The Docker Compose template for your services.
- `inventory.tpl`: The Ansible inventory template.
- `playbook.yml`: The Ansible playbook to install and configure the services.

## Explanation

Here is the block that run Ansible

```bash
deploy_ansible:
  stage: deploy_ansible
  image:
    name: alpinelinux/ansible
    entrypoint: [""]
  before_script:
    - cd ansible/
    - apk add gettext
    - pip3 install awscli
    - export MY_SECRET=$(aws secretsmanager get-secret-value --secret-id epita-secret --query 'SecretString' --output text --region us-east-1)
    - echo "$MY_SECRET" > secret_key.pem
    - chmod 400 secret_key.pem
    - export PUBLIC_IP=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=ec2-epita" "Name=instance-state-name,Values=running" --query "Reservations[].Instances[0].PublicIpAddress" --output text --region us-east-1)
    - envsubst < inventory.tpl > inventory
    - envsubst < docker-compose.yml.tpl '$ECR_REPO$PUBLIC_IP'> docker-compose.yml # Only variable started by $ and not ${}
    - export ANSIBLE_HOST_KEY_CHECKING=false
  script:
    - ansible-playbook -i inventory playbook.yml
```

Here is the explication for each step :

1. `cd ansible/`: Change the current working directory to the ansible folder containing the playbook and related files.
2. `apk add gettext`: Install the gettext package, which provides the envsubst command used for templating.
3. `pip3 install awscli`: Install the AWS CLI using pip.
4. `export MY_SECRET=$(aws secretsmanager get-secret-value --secret-id epita-secret --query 'SecretString' --output text --region us-east-1)`: Fetch the secret value from AWS Secrets Manager and store it in the MY_SECRET environment variable.
5. `echo "$MY_SECRET" > secret_key.pem`: Write the secret value (SSH private key) to a file named secret_key.pem.
6. `chmod 400 secret_key.pem`: Set the file permissions of secret_key.pem to read-only for the owner.
7. `export PUBLIC_IP=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=ec2-epita" "Name=instance-state-name,Values=running" --query "Reservations[].Instances[0].PublicIpAddress" --output text --region us-east-1)`: Retrieve the public IP address of the EC2 instance and store it in the PUBLIC_IP environment variable.
8. `envsubst < inventory.tpl > inventory`: Use the envsubst command to substitute environment variables in the inventory.tpl template file and create the final inventory file.
9. `envsubst < docker-compose.yml.tpl '$ECR_REPO$PUBLIC_IP'> docker-compose.yml`: Use the envsubst command to substitute the $ECR_REPO and $PUBLIC_IP variables in the docker-compose.yml.tpl template file and create the final docker-compose.yml file.
10. `export ANSIBLE_HOST_KEY_CHECKING=false`: Set the ANSIBLE_HOST_KEY_CHECKING environment variable to false to disable strict host key checking in Ansible, which simplifies the initial connection to the EC2 instance.

## Notes

- Make sure to set the required environment variables before running the playbook. - The playbook uses the lookup function to fetch the values of these variables.
- Ensure that you have appropriate access to the EC2 instance, including the SSH key and permissions.
- If you have any issues, check the firewall and security group settings on your EC2 instance to ensure that the required ports are open for access.
- The before_script and script sections provided in this README are designed for use in a CI/CD pipeline. If running the playbook manually, follow the steps outlined in the before_script section, and then run ansible-playbook -i inventory playbook.yml
