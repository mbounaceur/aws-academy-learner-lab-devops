stages:
  - infra_validate
  - infra_plan
  - infra_apply
  - build
  - test
  - release
  - deploy_validate
  - deploy_plan
  - deploy_apply
  - deploy_ansible

variables:
  VERSION: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  TF_VAR_environment: default
  TF_VAR_USER: ${GITLAB_USER_NAME}
  TF_VAR_COMMIT: ${CI_COMMIT_SHORT_SHA}
  TF_VAR_BRANCH: ${CI_COMMIT_BRANCH}
  TFDIR: terraform
  TF_INPUT: 0 # All inputs are disabled on CI environment
  PLAN: ${CI_COMMIT_SHORT_SHA}-${TF_VAR_environment}.tfplan
  AWS_DEFAULT_REGION: us-east-1
  ECR_REPO: 605776579979.dkr.ecr.us-east-1.amazonaws.com
  DOCKER_HOST: tcp://docker:2375 # For shared runner
  # DOCKER_HOST: tcp://localhost:2375 # For our custom runner without WSL
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

before_script:
  - cd ${TFDIR}/infra/
  - rm -rf .terraform
  - terraform --version
  - terraform init

infra-validate:
  stage: infra_validate
  script:
    - terraform validate

infra-plan:
  stage: infra_plan
  script:
    - terraform plan -out=${PLAN}
  dependencies:
    - infra-validate
  artifacts:
    expire_in: 2 hrs
    paths:
      - ${TFDIR}/infra/${PLAN}

infra-apply:
  stage: infra_apply
  script:
    - terraform apply ${PLAN}
  environment:
    name: ${TF_VAR_environment}
  allow_failure: false
  dependencies:
    - infra-plan
  when: manual

building-back:
  image: node:17-alpine
  stage: build
  before_script:
    - echo "Building back tier"
    - cd server/
  script:
    - npm install
  artifacts:
    expire_in: 2 hrs
    paths:
      - server/node_modules/

building-front:
  image: node:17-alpine
  stage: build
  before_script:
    - echo "Building back tier"
    - cd client/
  script:
    - npm install
  artifacts:
    expire_in: 2 hrs
    paths:
      - client/node_modules/

testing-back:
  image: node:17-alpine
  stage: test
  allow_failure: false
  dependencies:
    - building-back
  before_script:
    - echo "Testing back tier"
    - cd server/
    - echo "Export environment variables to be used by application and run tests for the server application"
  script:
    - npm run test

testing-front:
  image: node:17-alpine
  stage: test
  allow_failure: false
  dependencies:
    - building-front
  before_script:
    - echo "Testing front tier"
    - cd client/
  script:
    - npm run test

release-back:
  stage: release
  image: docker:20.10.16
  allow_failure: false
  dependencies:
    - testing-back
  services:
    - docker:dind
  before_script:
    - cd ${BUILD_CONTEXT}
    - apk add --no-cache curl jq python3 py3-pip
    - pip install awscli
    - aws ecr get-login-password | docker login --username AWS --password-stdin ${ECR_REPO}
  script:
    - docker build -t ${ECR_REPO}/${ECR_NAME}:${VERSION} -f ${DOCKERFILE_NAME} .
    - docker push ${ECR_REPO}/${ECR_NAME}:${VERSION}
    - docker tag ${ECR_REPO}/${ECR_NAME}:${VERSION} ${ECR_REPO}/${ECR_NAME}:latest
    - docker push ${ECR_REPO}/${ECR_NAME}:latest
  variables:
    DOCKERFILE_NAME: Dockerfile
    ECR_NAME: ecr-back
    BUILD_CONTEXT: server

release-front:
  stage: release
  image: docker:20.10.16
  allow_failure: false
  dependencies:
    - testing-front
  services:
    - docker:dind
  before_script:
    - cd ${BUILD_CONTEXT}
    - apk add --no-cache curl jq python3 py3-pip
    - pip install awscli
    - aws ecr get-login-password | docker login --username AWS --password-stdin ${ECR_REPO}
  script:
    - docker build -t ${ECR_REPO}/${ECR_NAME}:${VERSION} -f ${DOCKERFILE_NAME} .
    - docker push ${ECR_REPO}/${ECR_NAME}:${VERSION}
    - docker tag ${ECR_REPO}/${ECR_NAME}:${VERSION} ${ECR_REPO}/${ECR_NAME}:latest
    - docker push ${ECR_REPO}/${ECR_NAME}:latest
  variables:
    DOCKERFILE_NAME: Dockerfile
    ECR_NAME: ecr-front
    BUILD_CONTEXT: client

validate:
  stage: deploy_validate
  before_script:
    - export AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    - cd ${TFDIR}/app/
    - rm -rf .terraform
    - terraform --version
    - terraform init
  script:
    - terraform validate
  variables:
    PLAN: ${CI_COMMIT_SHORT_SHA}-${TF_VAR_environment}-app.tfplan

plan:
  stage: deploy_plan
  before_script:
    - export AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    - cd ${TFDIR}/app/
    - rm -rf .terraform
    - terraform --version
    - terraform init
  script:
    - terraform plan -out=${PLAN}
  dependencies:
    - validate
  artifacts:
    expire_in: 2 hrs    
    paths:
      - ${TFDIR}/app/${PLAN}

apply:
  stage: deploy_apply
  before_script:
    - export AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    - export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    - cd ${TFDIR}/app/
    - rm -rf .terraform
    - terraform --version
    - terraform init
  script:
    - terraform apply ${PLAN}
  environment:
    name: ${TF_VAR_environment}
  allow_failure: false
  dependencies:
    - plan
  when: manual

deploy_ansible:
  stage: deploy_ansible
  image:
    name: alpinelinux/ansible
    entrypoint: [""]
  before_script:
    - cd ansible/
    - apk add gettext
    - pip3 install awscli
    - export MY_SECRET=$(aws secretsmanager get-secret-value --secret-id epita-secret --query 'SecretString' --output text --region us-east-1)
    - echo "$MY_SECRET" > secret_key.pem
    - chmod 400 secret_key.pem
    - export PUBLIC_IP=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=ec2-epita" "Name=instance-state-name,Values=running" --query "Reservations[].Instances[0].PublicIpAddress" --output text --region us-east-1)
    - envsubst < inventory.tpl > inventory
    - envsubst < docker-compose.yml.tpl '$ECR_REPO$PUBLIC_IP'> docker-compose.yml # Only variable started by $ and not ${}
    - export ANSIBLE_HOST_KEY_CHECKING=false
  script:
    - ansible-playbook -i inventory playbook.yml
