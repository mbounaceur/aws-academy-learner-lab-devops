data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    region         = "us-east-1"
    bucket         = "mehdibounaceur-epita-devops-appingi-2024"
    key            = "epita/bounac_m/infra.tfstate"
    dynamodb_table = "mehdibounaceur-epita-devops-appingi-2024"
  }
}

data "aws_ami" "latest_amazon_linux_2023" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["amazon"]
}