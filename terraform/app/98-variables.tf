variable "region" {
  description = "Region used for deploying on AWS."
  type        = string
  default     = "us-east-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "iam_instance_profile" {
  default = "LabInstanceProfile"
}

variable "tags" {
  description = "Tags mappings"
  type        = map(any)
  default = {
    SCHOOL = "EPITA",
    PROMO  = "APPING I 2021-2024"
    GROUP  = "1"
  }
}

variable "COMMIT" {
  type = string
}

variable "BRANCH" {
  type = string
}

variable "USER" {
  type = string
}