# Terraform AWS EC2 Instance Deployment

This Terraform module deploys an EC2 instance on AWS, complete with a security group, key pair, and a secret in AWS Secrets Manager to store the private key.

## Prerequisites

- Terraform 1.4.2
- AWS Provider 4.63.0
- Random Provider 3.5.1

## Usage

1. Clone the repository to your local machine.

2. Set the environment variables `TF_VAR_COMMIT`, `TF_VAR_USER`, and `TF_VAR_BRANCH` in your CI/CD system, or export them locally if running Terraform manually:

```bash
export TF_VAR_COMMIT=<your_commit_hash>
export TF_VAR_USER=<your_username>
export TF_VAR_BRANCH=<your_branch_name>
```

3. Initialize the Terraform backend:

```bash
terraform init
```

4. Apply the Terraform configuration:

```bash
terraform apply
```

## Variables

| Name                | Description                                          | Type   | Default             |
|---------------------|------------------------------------------------------|--------|---------------------|
| region              | AWS region for deployment.                           | string | "us-east-1"         |
| instance_type       | AWS EC2 instance type.                               | string | "t2.micro"          |
| iam_instance_profile| ARN of the IAM instance profile to attach to EC2.    | string | LabInstanceProfile   |
| tags                | Map of tags to apply to all resources.               | map    | SCHOOL, PROMO, GROUP|

## Outputs

- EC2 Instance: The EC2 instance created by the module.
- Security Group: The security group applied to the EC2 instance.
- Key Pair: The key pair used for SSH access to the EC2 instance.
- Secret: The AWS Secrets Manager secret storing the private key.

## Notes

- The COMMIT, USER, and BRANCH variables are fetched from the environment variables TF_VAR_COMMIT, TF_VAR_USER, and TF_VAR_BRANCH, respectively. These should be set in your CI/CD system, or exported locally if running Terraform manually.
- The IAM Instance Profile is set to an existing role, as this module does not create a new IAM role. Update the iam_instance_profile variable as needed.
- The VPC and subnet information is fetched from the Terraform remote state.
- If you run `terraform destroy`, you will not be able to recreate AWS Secret Managers because by default, there is a retention policy. You must run this command 

```bash
aws secretsmanager get-secret-value --secret-id <SECRET-ID> --query 'SecretString' --output text --region us-east-1
```
