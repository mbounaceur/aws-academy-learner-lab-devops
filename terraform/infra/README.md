# Terraform AWS Infrastructure Deployment

This Terraform code is used to deploy AWS infrastructure resources including VPC, and ECR repositories for the frontend and backend applications.

## Prerequisites

To use this code, you will need to have the following installed:

- [Terraform](https://www.terraform.io/downloads.html) version `1.4.2` or higher
- [AWS CLI](https://aws.amazon.com/cli/) configured with valid credentials
- [AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html) and [DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html) created

## Deployment Steps

1. Clone this repository to your local machine.
2. Navigate to the root directory of the cloned repository.
3. Run `tfswitch` to match right terraform version.
4. Initialize the backend and provider by running the following command: `terraform init`
5. Update `00-main.tf` with your own S3 Bucket and DynamoDB. You can use any key you want as long as it ends with tfstate
6. Review the `variables.tf` file to make any necessary changes to the default values.
7. Run `terraform plan` to see a preview of the resources that will be created.
8. If the preview looks correct, apply the changes by running the following command: `terraform apply`
9. Confirm the changes by typing `yes` when prompted.

## Terraform Modules

The Terraform code uses two modules to create the VPC and the ECR repositories.

- The `terraform-aws-modules/vpc/aws` module is used to create the VPC with three private subnets and three public subnets across three availability zones in the specified region.

- The `terraform-aws-modules/ecr/aws` module is used to create two ECR repositories, one for the frontend application and one for the backend application. A lifecycle policy is applied to the repositories to keep only one image and expire the rest.

## Variables

The following variables are defined in `variables.tf`:

- `region`: The AWS region where the resources will be created. The default value is `us-east-1`.
- `tags`: A map of tags to apply to the created resources.
- `ecr-front`: The name of the ECR repository for the frontend application.
- `ecr-back`: The name of the ECR repository for the backend application.
- `USER`: The name of Gitlab user retrieve with TF_VAR_USER
- `BRANCH`: The name of Gitlab branch retrieve with TF_VAR_BRANCH
- `COMMIT`: The name of Gitlab commit retrieve with TF_VAR_COMMIT

See more for [TF_VAR_name](https://developer.hashicorp.com/terraform/cli/config/environment-variables#tf_var_name)

## Outputs

The following outputs are defined in `outputs.tf`:

- `vpc_id`: The ID of the created VPC.
- `vpc_public_subnets`: The public subnets created in the VPC.
- `ecr_front_id`: The URL of the frontend ECR repository.
- `ecr_back_id`: The URL of the backend ECR repository.

## Backend Configuration

The Terraform backend is configured to use Amazon S3 to store the Terraform state file and Amazon DynamoDB to lock the state file for mutual exclusion. The configuration is defined in the `terraform` block in the code.
