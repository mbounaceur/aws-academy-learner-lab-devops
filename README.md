# AWS Academy Learner Lab Devops

## Prerequisites

1. Gitlab Account
2. [Docker](https://www.docker.com/) (for testing and if we used our own runner) 
3. Register to Academy Learner Lab
4. [MongoDB instance](https://cloud.mongodb.com) on the Free Tier
5. [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
6. [TFSwitch](https://tfswitch.warrensbox.com/Install/) or [Terraform 1.4.2 version]([fter](https://releases.hashicorp.com/terraform/1.4.2/))

## AWS Academy Learner Lab and its limitations

This course is specifically intended for Devops running on the [AWS Academy Learner Lab](https://d1.awsstatic.com/AWS%20Academy%20Learner%20Lab%20Educator%20Guide.pdf).

Thanks to AWS Academy Learner Lab, each student register in the course have a $100 AWS Platform Credit to use for the duration of the class.

As we have some limitation, we must therefore exercise caution to avoid charges that would deplete our budget too quickly. If we exceed the budget, we will lose access to our environment, along with all of your work.

Each session lasts 4 hours by default, but we can extend it by pressing the Start button to reset the timer. At the end of each session, all the resources we have created will be retained. However, we automatically terminate EC2 instances. Other resources, such as RDS instances, continue to work. Keep in mind that AWS doesn't stop certain AWS functions, so they may continue to incur charges between sessions. For example, an Elastic Load Balancer service or a NAT gateway. It may be a good idea to delete these types of resources and recreate them as needed to test your work during a session. We will have access to this environment for the duration of the training for which you are registered. Access to the workshop is interrupted at the end of the training.

To start the lab, we must access to the invitation we sent you by mail and click on Start. Then, we need to go in Modules -> Students Workshop -> Start Lab.

Then, to have access to AWS CLI variables and AWS console, we must go in AWS Details.

You can find more useful information on lab restriction in Readme tab or [here](https://cyberlab.pacific.edu/courses/comp175/resources/aws-academy/AWS_Academy_Learner_Lab-Foundational_Services.pdf).

For example, all service access is limited to the us-east-1 and us-west-2 Regions unless mentioned otherwise in the service details below. If we load a service console page in another AWS Region we will see access error messages.

Finally, you can press Reset button to delete all contents on AWS.

## Gitlab

Thanks to Gitlab SaaS free tiers, we have compute credits limit of 400 minutes per month. If we exceed this limitation, we can create our own [Gitlab Runner](https://kyosenaharidza.medium.com/how-to-create-your-own-gitlab-runner-a37cd54bae33) to have unlimited compute credits limit.

⬇️ You can see below some useful Gitlab documentation link : ⬇️

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

# Configuration

## Gitlab Secrets

First, we will use [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) and more specifically [AWS Envionment variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) in our [Gitlab Protected variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) in order to have Gitlab pipeline interracting with AWS.

In our project, we will go in Settings -> CI/CD -> Variables -> Add variable and then add protected and masked variable to our pipeline.

⚠️ It's important to never commit your AWS ACCESS KEY and SECRET KEY to a public Git repository or any other version control service. These sensitive pieces of information can be used to access your AWS account and potentially perform unauthorized actions on your AWS resources. It's recommended to store them securely and use them with caution. If you suspect they have been compromised, it's recommended to revoke them immediately and generate new keys to protect your AWS account.

![alt text](doc/gitlab_variables.jpg "Gitlab AWS Protected Masked variables")

We will also add these environment variables:

1. MONGODB_URI (Ex: **mongodb+srv://<user>:<password>@epita.n1ypyi1.mongodb.net/**) This variable can be retrieve in [Mongo DB](https://cloud.mongodb.com) -> Database -> <YOUR DATABASE> -> Connect -> Drivers
![alt text](doc/mongo-driver.jpg "Mongo Cloud")
As Gitlab Secrets needs to matches some arbitary regex (see [link](https://gitlab.com/gitlab-org/gitlab/-/issues/267348)), we can remove **?retryWrites=true&w=majority** as "?" doesn't match some arbitary regex. If password has some variables that not match arbitary regex, we will not be able to masked the variable.
2. EMAIL : Email that will be used for login to the application
3. PASSWORD : Password that will be used for login to the application
4. TOKEN_KEY : Random string secret key that permit to sign [JWT Token](https://fr.wikipedia.org/wiki/JSON_Web_Token). In a nutshell, the main purpose of a JWT is to enable authentication and authorization between different applications or services.

## S3 Bucket and DynamoDB

The only ressources that we will create manually is an [S3 bucket](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html) and [DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html). This AWS S3 bucket will be used to store Terraform state for our project.

Amazon S3 (Simple Storage Service) is a highly scalable and secure object storage service provided by Amazon Web Services. It allows you to store and retrieve data from anywhere on the web, and offers features such as versioning, encryption, and lifecycle policies to help you manage your data efficiently. Each object stored in S3 has a unique name, and S3 buckets - the containers for storing objects - also have unique names, making it easy to access and manage your data.

Simply create S3 in the AWS Console.

For AWS DynamoDB, choose name whatever you want (not unique name). For Partition key, use LockID as string. We're using DynamoDB for [State Locking](https://developer.hashicorp.com/terraform/language/settings/backends/s3#dynamodb-state-locking). DynamoDB table will be used for locking to prevent concurrent operations on a single workspace.
![alt text](doc/dynamodb-LockID.jpg "DynamoDB Lock ID")

If we encounter some lock problem because of an error. We can unlock the state throught this command line

```bash
terraform force-unlock [options] LOCK_ID
```

[Here](https://developer.hashicorp.com/terraform/cli/commands/force-unlock) is the documentation for force-unlock

## TFSwitch

With TFSwitch, Terraform is by default installed on $HOME/bin.
You can add this path in your .zshrc or .bashrc depending on the bash that you're using.

```bash
if [[ $SHELL == *"zsh"* ]]; then
  echo "Writing in .zshrc.."
  echo "export PATH=\$PATH:$HOME/bin" >> ~/.zshrc
else
  echo "Writing in .bashrc"
  echo "export PATH=\$PATH:$HOME/bin" >> ~/.bashrc
fi
```

## AWS CLI

Get the AWS CLI in AWS Details (in AWS Academy Learner Labs) and paste it in ~/.aws/credentials

```bash
#!/bin/bash

# Create file if not exists in ~/.aws/credentials
if [ ! -d ~/.aws ]; then
  mkdir ~/.aws
fi
touch ~/.aws/credentials # Create if not exists
```

Here a screenshot where you can get the AWS CLI Variables

![alt text](doc/akskst.jpg "AWS CLI")

## Gitlab Runner

In order to use our own Gitlab runner instead of Gitlab shared runners because of some restrictions (or to avoid entering our credit card information), we need to install it.
In our project, we will go in Settings -> CI/CD -> Runners -> Show runner installation instructions.

Here is an example on Linux Environment

```bash
# Retrieve Registration Token
export REGISTRATION_TOKEN=<REGISTRATION_TOKEN>

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

sudo gitlab-runner register --non-interactive --run-untagged --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN --description "EPITA" --tag-list "EPITA" --executor "docker" --docker-privileged --docker-image alpine:latest
```

Then, once your runner is registered, you must disable "Enable shared runners for this project"

⚠️ You need to update your docker-in-docker (dind) services in case you're using your own runner ⚠️

If you're using Docker Desktop, modify the config.toml located in `/etc/gitlab-runner/config.toml`. You probably need root access to modify this file.

Then, in `[[runners.docker]]` section, add this line `host = "unix:///var/run/docker.sock"`

```bash

concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "EPITA"
  url = "https://gitlab.com/"
  id = 22802876
  token = "<TOKEN>"
  token_obtained_at = 2023-04-19T20:48:54Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    host = "unix:///var/run/docker.sock" # For WSL2 with Docker Destkop, add this line
    tls_verify = false
    image = "alpine:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0

```

Use `gitlab-runner verify` if your runner is not online in order to set it online.

Be sure to start your gitlab-runner with `gitlab-runner start` if your runner is online but your pipeline is on hold.

For Linux or Windows, you can use `DOCKER_HOST: tcp://localhost:2375` in your .gitlab-ci.yml according to your Docker configuration.

As we have a lot of image running on our own host, we can free some space by using this command `docker image prune -a`

# To go further

- Deploy SonarQube in an EC2
- Deploy Nexus to push some artefacts
- Deploy Hashicorp Vault to handle secrets
